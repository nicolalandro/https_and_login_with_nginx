# Serve with nginx
This repository show how to configure nginx to serve an http server ([NVIDIA Digits](https://github.com/NVIDIA/nvidia-docker/wiki/DIGITS)) adding the feature:
- [x] login
- [x] https

# How to use

## add password
    $ cd security
    $ docker-compose run htpassword
    $ # if you want to add your custom user and pass open htpasswd/docker-compose.yml and change the env variable
## create https certificate
    $ cd security
    $ docker-compose run openssl
## run service
    $ cd ..
    $ docker-compose up
    $ firefox localhost:8282
